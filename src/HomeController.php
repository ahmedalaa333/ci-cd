<?php

declare(strict_types=1);

namespace Ahmed;

class HomeController
{
    public function index() {
        echo sprintf('Hello Ahmed - version %s', $_GET['version'] ?? '1');
    }
}
