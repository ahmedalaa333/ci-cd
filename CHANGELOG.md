<!--- BEGIN HEADER -->
# Changelog

All notable changes to this project will be documented in this file.
<!--- END HEADER -->

## [1.0.0](https://gitlab.com/ahmedalaa333/ci-cd/compare/323459e327ebe21fb87a26c384427d657c59b18a...v1.0.0) (2021-09-05)
### Features


##### Index

* 1st commit ([6178f2](https://gitlab.com/ahmedalaa333/ci-cd/commit/6178f2fad3049152ccfc1cf69f5c914e5e0173bd))

---

